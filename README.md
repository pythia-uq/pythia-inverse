![PyThia Logo Full](logo/logo_full_transparent.png)

# PyThia Inverse

This repository contains functionalities to solve the inverse problem of parameter reconstruction using [PyThia UQ Toolbox](https://pythia-uq.rtfd.io) as a forward model.

## Installation

To install PyThia Invese from source, i.e. if you want to work with the latest
(and possibly unstable) changes, simply clone the repository and run the setup
script to install to any environment
```shell
cd path/to/pythia_inverse/
pip install .
```
PyThia Inverse can then be imported from any location with `import pythia-inverse`.

## Documentation

## How to cite PyThia Inverse

There is no official related article to cite PyThia Inverse yet.
If you make use of PyThia in a publication, please cite it with a BibTeX entry similar to this:
```bibtex
@misc{pythia-inverse,
    author = {Farchmin, Nando},
    title = {PyThia Inverse Toolbox},
    howpublished = {vers.: 0.1},
    note = {\url{https://gitlab1.ptb.de/pythia/pythia/}},
    year = {2022},
    month = {12}
}
```

## Want to contribute?

Check out the [contribution guidelines](CONTRIBUTING.md) on how to create
issues or file bug reports and feature requests.
Or ever better start developping the PyThia project yourself after reading the
[development guidelines](DEVELOPERS.md).
