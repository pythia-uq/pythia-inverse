import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pythia-inverse",
    version="1.0.0",
    author="Nando Farchmin",
    author_email="nando.farchmin@ptb.de",
    description=("Package for solving inverse problems and quantifying "
                 + "uncertainties based on the pythia-uq package."),
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab1.ptb.de/pythia/pythia_inverse",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "numpy>=1.20.0",
        "scipy>=1.5.0",
        "pythia-uq>=3",
        "sphinx-autodoc-typehints>=1.18.1",
    ],
)
