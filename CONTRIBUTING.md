# Contributing to PyThia Inverse

We'd love for you to contribute to our source code and to make PyThia even
better than it is today! Here are the guidelines we'd like you to follow:

* [Code of Conduct](#coc)
* [Questions and Problems](#question)
* [Issues and Bugs](#issue)
* [Feature Requests](#feature)
* [Improving Documentation](#docs)
* [Issue Submission Guidelines](#submit)
* [Merge Request Submission Guidelines](#submit-pr)

## <a name="coc"></a> Code of Conduct

Help us keep PyThia Inverse open and inclusive. Please read and follow our
[Code of Conduct](CODE_OF_CONDUCT.md).

## <a name="requests"></a> Questions, Bugs, Features

### <a name="question"></a> Got a Question or Problem?

Do not open issues for general support questions as we want to keep GitHub
issues for bug reports and feature requests. You've got much better chances of
getting your question answered on dedicated support platforms, the best being
[Stack Overflow][stackoverflow].

Stack Overflow is a much better place to ask questions since:

- there are thousands of people willing to help on Stack Overflow
- questions and answers stay available for public viewing so your question /
  answer might help someone else
- Stack Overflow's voting system assures that the best answers are prominently
  visible.

To save your and our time, we will systematically close all issues that are
requests for general support and redirect people to the section you are reading
right now.

**Note: Currently there exists not dedicated Thread for PyThia on Stack Overflow!**

### <a name="issue"></a> Found an Issue or Bug?

If you find a bug in the source code, you can help us by submitting an issue to
our [GitLab Repository][gitlab]. Even better, you can submit a merge request
with a fix.

**Please see the [Submission Guidelines](#submit) below.**

### <a name="feature"></a> Missing a Feature?

You can request a new feature by submitting an issue to our
[GitLab Repository][gitlab-issues].

If you would like to implement a new feature then consider what kind of change
it is:

- **Major Changes** that you wish to contribute to the project should be
  discussed first in a [GitLab issue][gitlab-issues] that clearly outlines the
  changes and benefits of the feature.
- **Small Changes** can directly be crafted and submitted to the
  [GitLab Repository][gitlab] as a merge request.
  See [core development documentation][developers] for detailed information.

### <a name="docs"></a> Want a Doc Fix?

Should you have a suggestion for the documentation, you can open an issue and
outline the problem or improvement you have - however, creating the doc fix
yourself is much better!

If you want to help improve the docs, it's a good idea to let others know what
you're working on to minimize duplication of effort. Create a new issue (or
comment on a related existing one) to let others know what you're working on.

If you're making a small change (typo, phrasing) don't worry about filing an
issue first.

For large fixes, please build and test the documentation before submitting the
merge request to be sure you haven't accidentally introduced any layout or formatting
issues. You should also make sure that your commit message follows the
**[Commit Message Guidelines][developers.commits]**.

## <a name="submit"></a> Issue Submission Guidelines
Before you submit your issue search the archive, maybe your question was
already answered.

If your issue appears to be a bug, and hasn't been reported, open a new issue.
Help us to maximize the effort we can spend fixing issues and adding new
features, by not reporting duplicate issues.

The "[new issue][gitlab-new-issue]" form contains a number of prompts that you should fill out to
make it easier to understand and categorize the issue.

In general, providing the following information will increase the chances of
your issue being dealt with quickly:

- **Overview of the Issue** - if an error is being thrown a non-minified stack trace helps
- **Motivation for or Use Case** - explain why this is a bug for you
- **PyThia Inverse Version(s)** - is it a regression?
- **Reproduce the Error** - provide a live example or an unambiguous set of steps.
- **Related Issues** - has a similar issue been reported before?
- **Suggest a Fix** - if you can't fix the bug yourself, perhaps you can point to what might be causing the problem (line of code or commit)

## <a name="submit-pr"></a> Merge Request Submission Guidelines

- Search for an open or closed merge request that relates to your submission.
  You don't want to duplicate effort.
- Make your changes in a new git branch:

    ```shell
    git checkout -b my-fix-branch development
    ```

- Create your patch commit, **including appropriate test cases**.
- Follow our [Coding Rules][developers.rules].
- If the changes affect public APIs, change or add relevant [documentation][developers.documentation].
- Commit your changes using a descriptive commit message that follows our
  [commit message conventions][developers.commits].
- Before creating the merge request, package and run all tests a last time.
- Push your branch to GitLab:

    ```shell
    git push origin my-fix-branch
    ```

- In GitLab, send a merge request to `pythia:development`.
- If you find that the continuous integration tests have failed, look into the
  logs to find out if your changes caused test failures, the commit message was
  malformed etc. If you find that the tests failed or times out for unrelated
  reasons, you can ping a team member so that the build can be restarted.
- If we suggest changes, then:
  - Make the required updates.
  - Re-run the PyThia Inverse test suite to ensure tests are still passing.
  - Commit your changes to your branch (e.g. `my-fix-branch`).
  - Push the changes to your GitLab repository (this will update your merge request).

That's it! Thank you for your contribution!

#### After your merge request is merged

After your merge request is merged, you can safely delete your branch and pull
the changes from the main (upstream) repository:

- Your feature branch should be deleted upon merge.
- In your local repository, change to the `development` branch and pull the latest changes:
    ```shell
    git checkout development
    git pull
    ```
- Also update the remote branch information, i.e. that your feature branch has been deleted:
    ```shell
    git fetch -p
    ```
- Delete your local branch:
    ```shell
    git branch -d my-fix-branch
    ```

[developers]: DEVELOPERS.md
[developers.commits]: DEVELOPERS.md#commits
[developers.documentation]: DEVELOPERS.md#documentation
[developers.rules]: DEVELOPERS.md#rules
[developers.setup]: DEVELOPERS.md#setup
[gitlab-issues]: https://gitlab1.ptb.de/farchm01/pythia/-/issues
[gitlab-new-issue]: https://gitlab1.ptb.de/farchm01/pythia/-/issues/new
[gitlab]: https://gitlab1.ptb.de/farchm01/pythia
[stackoverflow]: https://stackoverflow.com/
