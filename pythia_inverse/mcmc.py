import numpy as np
import pythia as pt


def random_walk_metropolis(density, domain, seeds, chain_length, delta):
    """Random walk Metropolis-Hastings algorithm for sampling densities.

    Since the random walks for multiple seeds are computed in parallel, the
    seed length of each seed is chosen uniformly.

    .. note::
        This is just for benchmarking MCMC sampling. Use other python libraries
        for reasonable computations.

    Parameters
    ----------
    density : function
        Density (not necessarily normalized) from which the samples are
        generated.
    seeds : array_like, ndim = 2
        Seed for stochastic sampling. First dimension is the number of seeds
        used for parallel sampling, second dimension is the dimension of the
        samples/parameters.
    chain_length : int
        Length of the Markov chains.
    delta : array_like
        Standart deviation of noise direction added in each step for each
        parameter.

    """
    if seeds.ndim < 2:
        seeds.shape = 1, -1
    assert seeds.ndim == 2
    n_seeds, dim = seeds.shape

    val = np.zeros([chain_length, n_seeds, dim])
    val[0] = seeds
    rejections = np.zeros(n_seeds)
    threshold = np.random.uniform(0, 1, [chain_length, n_seeds])
    x = density(val[0])
    perturbation = np.zeros([n_seeds, dim])

    # NOTE start with j=1 since val[0] is seed!
    for j in range(1, chain_length):
        for j_seed in range(n_seeds):
            while True:
                EPS = np.random.normal(0, delta)
                perturbation[j_seed] = val[j-1, j_seed] + EPS
                if pt.misc.is_contained(perturbation[j_seed], domain):
                    break
        y = density(perturbation)
        for j_seed in range(n_seeds):
            # alpha = y[j_seed] / x[j_seed] if x[j_seed] > 0 else 1
            if threshold[j, j_seed]*x[j_seed] <= y[j_seed]:
                val[j, j_seed] = perturbation[j_seed]
                x[j_seed] = y[j_seed]
            else:
                rejections[j_seed] += 1
                val[j, j_seed] = val[j-1, j_seed]

    return val.swapaxes(0, 1), rejections
